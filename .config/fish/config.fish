# pyenv
set -Ux PYENV_ROOT $HOME/.pyenv
set -U fish_user_paths $PYENV_ROOT/bin $fish_user_paths

status is-interactive; and pyenv init --path | source
pyenv init - | source
status --is-interactive; and pyenv virtualenv-init - | source

alias rgdjango="rg -g '!tests/**' -g '!*/**/tests/**' -g '!fixtures/*' -g '!*/**/fixtures/*' -g '!migrations/*' -g '!*/**/migrations/*'"
alias rgvendor="rg -g '!**/vendor'"

# abbrevations
abbr -a dc docker compose
abbr -a dce docker compose exec
abbr -a dcu docker compose up
abbr -a dcb docker compose build
abbr -a dcl docker compose logs
abbr -a dcs docker compose stop
abbr -a ll eza -lag
abbr -a cat bat
abbr -a gs git status
abbr -a gc git commit -vs
abbr -a gl "git log --oneline --graph --decorate --date-order --all --date=format-local:'%Y-%m-%d %H:%M' --pretty=format:'%C(auto)%h%d %s %Cblue[%ad by %an]%Creset'"
abbr -a fd fd -I
abbr -a debugpy python -m debugpy --wait-for-client --listen 12345
abbr -a azurefunc 'azurite --silent &; func host start --functions dev_azure_function --language-worker -- "-m debugpy --listen 12345"'

# Nvim
set -gx MYVIMRC "$HOME/.config/nvim/init.lua"

# PHP
set PATH "$HOME/.config/composer/vendor/bin" $PATH

# Fly.io
set -gx FLYCTL_INSTALL "$HOME/.fly"
set PATH "$FLYCTL_INSTALL/bin" $PATH

# Nodejs
set -gx NVM_DIR "$HOME/.nvm"

# QT
set -gx QT_STYLE_OVERRIDE adwaita
set -gx QT_QPA_PLATFORM wayland
set -gx QT_QPA_PLATFORMTHEME qt5ct

# ranger
set -gx RANGER_LOAD_DEFAULT_RC "FALSE"

# kitty
set PATH "$HOME/.local/bin" $PATH
set PATH "$HOME/.local/share/applications" $PATH

# go
set GOPATH "$HOME/go"
set PATH /usr/local/go/bin $PATH
set PATH "$GOPATH/bin" $PATH

# Neo vim
set -gx VISUAL nvim
set -gx EDITOR $VISUAL

# Browser
# set -gx BROWSER firefox
set -gx BROWSER brave-browser
set -gx MOZ_ENABLE_WAYLAND 1
set -gx MOZ_DBUS_REMOTE 1

# GPG and SSH
# set -x GPG_TTY (tty)
# set -x SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
# gpgconf --launch gpg-agent

# snap
set snap_bin_path "/var/lib/snapd/snap/bin"
set PATH /var/lib/snapd/snap/bin $PATH

# Flatpaks
set PATH /var/lib/flatpak/exports/bin $PATH
set PATH $HOME/.local/share/flatpak/exports/share $PATH

# Appimages
set PATH /opt/appimages $PATH
set PATH $HOME/Applications $PATH

# ripgrep
set -gx RIPGREP_CONFIG_PATH "$HOME/.config/ripgrep/.ripgreprc"

set -gx _JAVA_AWT_WM_NONREPARENTING 1

set PPID (ps --pid %self -o ppid --no-headers)
set PPID (string trim $PPID)
set -gx KITTY_LISTEN_ON "unix:/tmp/mykitty-$PPID"


if test "$TERM" = "xterm-kitty"
    alias ssh="env TERM=xterm-256color ssh"
end
