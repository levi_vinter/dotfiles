function git_commit_hash
    set commit_message $argv[1]
    set head_limit $argv[2]
    set commit_hash (git log | head -$head_limit | grep "$commit_message" | awk 'NR==1{print $1 }' | tr -d "\n")
    if test -z $commit_hash
        echo "No commit hash with message $commit_hash"
    else
        echo $commit_hash | tr -d "\n" | xsel --clipboard
        set commit_lines (git log | head -$head_limit | grep "$commit_message")
        echo "$commit_lines"
    end
end
