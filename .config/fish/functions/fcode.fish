function fcode
    if set -q argv[1]
        set max_depth $argv[1]
    else
        set max_depth 3
    end

    set search_dir $HOME/Dev/

    switch (echo $max_depth)
    case whmcs
        set max_depth 3
        set search_dir $HOME/Dev/whmcs/whmcs-dev/whmcs_repos/
    end

    if set -q argv[2]
        set search_dir $argv[2]
    end

    set -l dest_dir (find $search_dir -maxdepth "$max_depth" -type d -not -iwholename "*.git*" -printf '%P\n' | fzf -i)
    echo $search_dir/$dest_dir

    if test -z "$dest_dir"
        return 1
    end

    code $search_dir/$dest_dir
end
