function foath
    set -l ykman_oath (ykman oath accounts list | fzf)
    ykman oath accounts code -s $ykman_oath | tr -d "\n" | wl-copy
end
