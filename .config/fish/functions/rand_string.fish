function rand_string
    if set -q argv[1]
        set string_length (math "$argv[1] + 1")
    else
        set string_length (math "40 + 1")
    end

    set random_string (tr -cd '[:alnum:]' < /dev/urandom | fold -w $string_length | head -n 1)
    echo $random_string
    echo $random_string | tr -d '\n' | wl-copy
end
