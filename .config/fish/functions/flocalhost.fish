function flocalhost
    set -l localhost (grep -Po '127.0.0.1 +\K[\w.\-_]+' /etc/hosts | fzf -i)
    echo "$localhost"
    echo "$localhost" | tr -d "\n" | xsel --clipboard
end
