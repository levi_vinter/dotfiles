#!/bin/bash

# Make your script exit when a command fails.
# Add || true to commands that you allow to fail.
# set -e
# Exit when using undeclared variables.
# set -u
# Catch pipe fails in e.g. mysqldump | gzip.
# set -o pipefail
# Trace each command in script.
# set -x

(
  flock 200

  killall -q polybar

  while pgrep -u $UID -x polybar > /dev/null; do sleep 0.5; done

  outputs=$(xrandr --query | grep " connected" | cut -d" " -f1)
  tray_output=eDP-1

  for m in $outputs; do
    if [[ $m == "DP-1" ]]; then
        tray_output=$m
    fi
  done

  for m in $outputs; do
    export WIRELESS=$(ip link show | grep "<BROADCAST,MULTICAST,UP,LOWER_UP>" | awk 'NR==1{print $2}' | tr -d ':')
    export MONITOR=$m
    export TRAY_POSITION=none
    if [[ $m == $tray_output ]]; then
      TRAY_POSITION=right
    fi

    polybar --reload mainbar-i3 </dev/null >/var/tmp/polybar-$m.log 2>&1 200>&- &
    disown
  done
) 200>/var/tmp/polybar-launch.lock
