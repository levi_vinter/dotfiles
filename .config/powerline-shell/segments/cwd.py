"""Segment that prints out the current working directory like fish shell."""

import os
import re
from powerline_shell.utils import py3, BasicSegment


class Segment(BasicSegment):
    def add_to_powerline(self):
        cwd = self.powerline.cwd
        if not py3:
            cwd = cwd.decode("utf-8")

        home = os.path.realpath(os.getenv("HOME"))
        if cwd.startswith(home):
            cwd = "~" + cwd[len(home) :]

        # Only first letter of parent directory names should be printed
        cwd = re.sub(r"\/(\.?[^/])[^/]+?(?=\/)", r"/\1", cwd, count=0)

        self.powerline.append(
            " %s " % cwd,
            self.segment_def.get("fg_color", self.powerline.theme.PATH_FG),
            self.segment_def.get("bg_color", self.powerline.theme.PATH_BG),
        )
