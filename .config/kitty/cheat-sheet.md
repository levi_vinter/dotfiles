# Kitty cheat-sheet

## Windows

New window

```
ctrl+shift+Enter
```

New OS window

```
ctrl+shift+n
```

Close window

```
ctrl+shift+w
```

Next window

```
alt+l
```

Previous window

```
alt+h
```

Zoom window

```
ctrl+shift+z
```

Move window forward

```
ctrl+shift+f
```

Move window backward

```
ctrl+shift+b
```

## Tabs

New tab

```
ctrl+shift+t
```

Next tab

```
ctrl+alt+l
```

Previous tab

```
ctrl+alt+h
```

Choose tab interactively

```
ctrl+alt+i
```

## Miscellenous

Open URL in browser ```ctrl+shift+e ```

Reload config ```ctrl+shift+F5```

Copy to buffer a ```f1```
Paste from buffer a ```f2```

Copy to buffer b ```f3```
Copy from buffer b ```f4```

Browse last cmd output ```ctrl+shift+g```
