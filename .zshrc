# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
#ZSH_THEME="robbyrussell"
#ZSH_THEME="spaceship"
ZSH_THEME="powerlevel10k/powerlevel10k"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"
DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"
HIST_STAMPS="yyyy-mm-dd"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git z)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
#

# Lines configured by zsh-newuser-install
setopt autocd extendedglob nomatch notify
unsetopt beep
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/thomas/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# History search
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "${key[Up]}"   ]] && bindkey -- "${key[Up]}"   up-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search


# pyenv
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

if [[ -n $commands[pyenv] ]]; then
  eval "$(pyenv init --path)"
  #eval "$(pyenv init -)"
  if [[ $commands[pyenv-virtualenv-init] ]]; then
    eval "$(pyenv virtualenv-init -)"
  fi
fi

# Abbreviations
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias dc='docker compose'
alias dce='docker compose exec'
alias dcu='docker compose up'
alias dcb='docker compose build'
alias dcl='docker compose logs'
alias dcs='docker compose stop'
alias ll='eza -lag'
# Check if the 'batcat' executable exists
if [[ -x "$(command -v batcat)" ]]; then
  alias bat='batcat'
fi
alias cat='bat'
alias gs='git status'
alias gc='git commit -vs'
alias gl="git log --oneline --graph --decorate --date-order --all --date=format-local:'%Y-%m-%d %H:%M' --pretty=format:'%C(auto)%h%d %s %Cblue[%ad by %an]%Creset'"
# Check if the 'fdfind' executable exists
if [[ -x "$(command -v fdfind)" ]]; then
  alias fd='fdfind -I -H'
else
    alias fd='fd -I -H'
fi
alias debugpy='python -m debugpy --wait-for-client --listen 12345'
alias azurefunc='azurite --silent &; func host start --functions dev_azure_function --language-worker -- "-m debugpy --listen 12345"'
alias awslogin='saml2aws login --skip-verify --force && eval $(saml2aws script)'
alias openfortigui="sudo sh -c 'nohup /usr/bin/openfortigui > ~/nohup.out 2>&1 &'"
alias tf='terraform'

# Environment Variables
#
# Snap
export PATH="/var/lib/snapd/snap/bin:$PATH"
# Flatpak
export PATH="/var/lib/flatpak/exports/bin:$PATH"
export PATH="$HOME/.local/share/flatpak/exports/share:$PATH"
# Appimages
export PATH="/opt/appimages:$PATH"
export PATH="$HOME/Applications:$PATH"
# PHP
export PATH="$HOME/.config/composer/vendor/bin:$PATH"
# Fly.io
export FLYCTL_INSTALL="$HOME/.fly"
export PATH="$FLYCTL_INSTALL/bin:$PATH"
# Nodejs
export NVM_DIR="$HOME/.nvm"
# QT
export  QT_QPA_PLATFORM="wayland"
#export QT_STYLE_OVERRIDE="adwaita"
#export QT_QPA_PLATFORM="wayland"
#export QT_QPA_PLATFORMTHEME="qt5ct"
# Ranger
export RANGER_LOAD_DEFAULT_RC="FALSE"
# Kitty
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.local/share/applications:$PATH"
# Golang
export GOPATH="$HOME/go"
export PATH="/usr/local/go/bin:$PATH"
export PATH="$GOPATH/bin:$PATH"
# Neovim
export PATH="/usr/local/nvim-linux64/bin:$PATH"
export MYVIMRC="$HOME/.config/nvim/init.lua"
export VISUAL="nvim"
export EDITOR="$VISUAL"
# Browser
export BROWSER="brave-browser"
export MOZ_ENABLE_WAYLAND=1
export MOZ_DBUS_REMOTE=1
# Terraforom
export PATH="$HOME/.tfenv/bin:$PATH"
# GPG and SSH
# export GPG_TTY=$TTY
# export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
# gpgconf --launch gpg-agent
# Ripgrep
export RIPGREP_CONFIG_PATH="$HOME/.config/ripgrep/.ripgreprc"

export _JAVA_AWT_WM_NONREPARENTING=1

# saml2aws
if [[ -x "$(command -v saml2aws)" ]]; then
    eval "$(saml2aws --completion-script-zsh)"
fi

# Get Parent Process ID (ParentPID) and trim whitespace
ParentPID=$(ps -o ppid= -p $$ | xargs)

# Export KITTY_LISTEN_ON with the ParentPID
export KITTY_LISTEN_ON="unix:/tmp/mykitty-$ParentPID"

if [[ "$TERM" == "xterm-kitty" ]]; then
  alias ssh="env TERM=xterm-256color ssh"
fi

# Add fish-like functionality
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
