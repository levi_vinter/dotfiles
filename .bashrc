#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
# PS1='[\u@\h \W]\$ '

# Aliases
PATH=~/.local/bin:$PATH
alias fcd='. fcd.sh'
alias dce='docker-compose exec'
alias dcu='docker-compose up'
alias rg='rg -S --follow --no-ignore-vcs'

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

# Kitty
export TERM=xterm
PATH="$HOME/.local/share/applications:$PATH"

# Browser
BROWSER=firefox

# Neo vim
VISUAL=nvim
EDITOR="$VISUAL"
# alias vim="nvim"

# go
PATH=/usr/local/go/bin:$PATH
PATH="$HOME/go/bin:$PATH"

# php
PATH="$HOME/.config/composer/vendor/bin:$PATH"

# Ranger
export RANGER_LOAD_DEFAULT_RC="FALSE"

# Dotfiles
alias config='/usr/bin/git --git-dir=$HOME/.cfg --work-tree=$HOME'

# SSH
# export GPG_TTY="$(tty)"
# export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
# gpgconf --launch gpg-agent

alias luamake=/home/thomas/.local/share/lua-language-server/3rd/luamake/luamake

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

